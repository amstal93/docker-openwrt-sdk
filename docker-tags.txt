# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# see Supplementary Files in https://downloads.openwrt.org/releases/21.02.0/targets/ath79/generic/
21.02.0-ath79-tiny	OPENWRT_ARCH=mips_24kc OPENWRT_GCC=8.4.0_musl OPENWRT_HOST=Linux-x86_64 OPENWRT_TARGET=ath79 OPENWRT_SUBTARGET=tiny OPENWRT_VERSION=21.02.0
21.02.0-ath79	OPENWRT_ARCH=mips_24kc OPENWRT_GCC=8.4.0_musl OPENWRT_HOST=Linux-x86_64 OPENWRT_TARGET=ath79 OPENWRT_SUBTARGET=generic OPENWRT_VERSION=21.02.0
